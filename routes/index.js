var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        'services': [
            {
                'icon': 'lni-cog',
                'headline': 'Bootstrap 4',
                'description': 'Share processes and data secure lona need to know basis Our team assured your web site is always safe.',
            },
            {
                'icon': 'lni-brush',
                'headline': 'Slick Design',
                'description': 'Share processes and data secure lona need to know basis Our team assured your web site is always safe.',
            },
            {
                'icon': 'lni-heart',
                'headline': 'Crafted with Love',
                'description': 'Share processes and data secure lona need to know basis Our team assured your web site is always safe.',
            },
        ]
    });
});

router.post('/', [
    check('name').not().isEmpty().withMessage('Please enter your name'),
    check('subject').not().isEmpty().withMessage('Please enter your subject'),
    check('email')
        .not().isEmpty().withMessage('Please enter your email').bail()
        .isEmail().withMessage('Please enter a valid email'),
    check('budget').not().isEmpty().withMessage('Please enter your budget'),
    check('message').not().isEmpty().withMessage('Write your message'),
], function(req, res, next) {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        return res.status(200).json({success: true});
    }
    return res.status(400).json({success: false, errors: errors.array()});
});

module.exports = router;

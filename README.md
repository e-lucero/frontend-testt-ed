# Frontend Test

## Requirements

* [Node](https://nodejs.org/)
* [NPM](https://www.npmjs.com/)

## Running

```sh
npm install
npm start
```

Visit http://localhost:3000/ in your browser.

## Development

All changes to Sass files are auto-compiled. Javascript files are not processed.

## Tasks

1. Fix the bug so the loading animation hides correctly.

2. Make the current header link styling animate when scrolling and hovering.

3. Replace all the primary colors (#3ecf8e) **and** shades in `main.scss` with a variable that can
   be changed.

4. Update the template to implement the new section shown, using the template context (do not
   hard-code): ![services.png](services.png)

5. Update the contact form to be AJAX, and display the server response messages on error or submit.
